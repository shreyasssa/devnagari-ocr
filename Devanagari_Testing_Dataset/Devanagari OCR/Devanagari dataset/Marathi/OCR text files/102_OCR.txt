टण.

आजचे काम मात्र त्यांच्यासाठी खेळासारखे नव्हते. लहान वयात
होस वाटते. एकच काम कंटाळबाणे वाटते. पण आज हे काम करणे पया
| समज त्यांना आली होती. हातापेक्षाही मन थकून गेले तरी थकावट दाखवायची
व्मले' हा शब्द उच्चारायचा नाही, असा त्यांचा मनोमन निश्चय होता आपल्या
ना त्यांच्या शक्‍तीबाहेरील कष्ट पडत आहे हे त्यांच्या आईला दिसत डं

   
  
   
  
  
  
  
  
   
  
   
  

« पण

स्व होते म्हणून मुलींसाठी कणव आणण्यास तिला दिबसभर बेळ नव्हता. कामाच्या
जामुळे आपल्याला दंड झाला तरी त्या विलंबाने काम देणागंची हानी भरून येणार
ही तिच्या मनाला टोचणी होती, कारण ते काम आपल्याच राष्ट्राच्या संरक्षण
तचे होते! देशाच्या तटबंदीला रचनात्मक पद्धतीने बळकटी आणण्याच्या योजनेला
हातभार लावीत होती!

ठरविल्याप्रमाणे दुसऱ्या दिवशी काम पूर्ण झाले होते. ते योग्य ठिकाणी पोहचविले
होते.

क्रांतिकारकांच्या वाट्याला बंदिवास आला की त्यांच्या नातेवाईकांच्या आणि
अवलंबितांच्या वाट्याला यातना येतात ही इतिहासाची परंपरा आहे. जवळच्या व्यक्‍ती
ंबंध ठेवीत नाहीत. असलेले संबंध तोडतात. क्रांतिकारकांच्या आप्तांशी आपण
तर, न जाणो, आपणास पकडून नेतील अशी भीती त्यांच्या मनात वावरत
असल्याने रस्त्यातून जाताना कधी समोरासमोर येऊनही ते ओळख दाखवीत नाहीत.
अशी भीती वाटण्यात त्या व्यक्तीचा दोष असतो असे नाही. भीतियुक्‍त वातावरण
निर्माण केले जाते त्याचा तो परिणाम असतो. बाळीत टाकणे ही प्रथा आजच्या लिखित
विधीला संमत नाही. तरीही, क्रांतिकारकांच्या अवलंबितांची कोंडी निर्माण व्हावी
अशा पद्धतीने विधीच्या संरक्षकांचा व्यवहार होतो. .

गांधीहत्येच्या निमित्ताने दहा सहल्न हिन्दुंना पकडले होते. आपला संबंध या हत्येशी

कशा प्रकारे जोडला जातो याचा प्रत्येकजण भीतियुक्‍त मनाने विचार करू लागला. हे
शोक पकडले जाण्याची निमितेही अनेक होती. काही तर आगवीच पुन्लक होती.
“णाच्या घरी संपूर्ण हिन्दुस्थानचे मानचित्र मिळाले. कोणाच्या घरात संघाची

होती कोणाजबळ सावरकरांचे एखादे पुस्तक मिळाले. कोणी आ म

ओण ती चित्रे लाबली होती. त्यात गांधीजींचे चित्र नव्हते. रले होते. कोणा
र गोडसे? हे क्रियाविशेषण वापरले होते.

वह तिरी लिहिलेल्या पत्रात 'गोडसे' हे दिसली, कोणाच्या घरी धुणी

घरात काठीला लावलेली भगवी पताका