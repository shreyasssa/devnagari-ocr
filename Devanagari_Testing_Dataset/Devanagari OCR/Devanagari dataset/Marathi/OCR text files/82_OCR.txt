1[1 771111 ढणणणममळळळााा०००० यावा.

“पातु मी दिलं आहे. द्रुपदीची आई गरीब आहे, मला माहीत आहे, '' आजी
म्हणाली.
*आणि तुम्ही का श्रीमंत आहात?'' मी विचारले.

*'आम्ही ख्ाऊन-पिऊन सुखी आहोत, श्याम, '' ती सहृदयतेने म्हणाली.

माझ्याने 'माही' म्हणवेना. माझा पाय दुखत होता. ठणकत होता. मी भाकरी
खाल्ली नि ताट उचलुन नेऊ लागलो; परंतु लंगडत होतो.

*श्याम, पायाला रे काय झालं?'' कनबाळूपणाने म्हातारबायने विचारले.

"याया मोठा काटा बोचलाय नि फार दुखतोय, '' मी म्हटले.

"कुठे गेला होतास रानावनात?''

"मी नाचणारे मोर पाहिले. नाचणारा मोर मी पूर्वी कधीच पाहिला नव्हता.
न्यामुळे उशीर झाला. पाऊस पडू लागला. रस्त्यात चिखल. बहाणा काढून घेतल्या,
तग काटा बोचला,'' मी इतिहास सांगितला.

*'आता रात्रीचा काढायला दिसणारही नाही. तेल-घडी बांधून ठेव, म्हणजे
फुगेल. मग सकाळी चांगला दिसेल. आण ते ताट, मी नेते.'' असे म्हणून तिने
माझ्या हातातले ताट घेतले.

मी घोंगडीवर पांघरूण घेऊन बसलो होतो. हवेत गारवा आला होता. म्हातारी
माझ्या खोलीत येऊन बसली.

*'आज वाचायचं नाही वाटते, श्याम?'' तिने विचारले.

"आज तुमचीच हकीगत सांगा. विटेगावच्या गोष्टी सांगा,'' मी म्हटले.

*'काय सांगू? तू येशील विटेगावला?'' तिने प्रेमाने विचारले.

*'तिथे काय आहे?'' मी विचारले.

"तिथे आम्ही दोन पिकली पानं आहोत. आमच्याकडे ये. तिथून पंढरीला जा.
आणि श्याम, आमच्या घरी एक मिठू आहे, तो 'विठठल विठ्ठल' म्हणतो. बारकरी

दिसले, की आमच्या मिठूचं भजन सुरू होतं. पहाट होताच 'विठ्ठल विठ्ठल' असा
विूच्या नामाचा गजर मिठू सुरू करतो. आता मिठू म्हातारा झाला आहे. आम्ही
म्हातारी, मिढूही म्हातारा, '' ती म्हणाली.

___ "मग विट्याला त्याची देखभाल कोण करीत असेल? कोण चण्याची डाळ
देईल? कोण पेरू देईल?'' मी विचारले.

जिथे तिथे पाय असे उभीख 4 ष्१