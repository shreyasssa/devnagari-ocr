आण्विक उस्त्रे नष्ट करण्यासाठी अमेरिका आणि रशिया
या देशांत २०१० मध्ये रिडक्शन अँड लिमिटेशन ऑफ
स्ट्रॅटेजिक ऑफेन्सिव्ह आर्म्स (आता एसटीएआरटी

- स्टार्ट) करार. त्याची मुदत फेब्रुवारी २०२१ मध्ये
संपणार. कराराला मुदतवाढीसाठीच्या २०१९ पासूनचे