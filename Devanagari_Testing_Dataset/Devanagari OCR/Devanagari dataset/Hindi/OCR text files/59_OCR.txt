भारतीय संस्कृति में हर एक दिन का एक महत्व
है, और हर मास का भी एक विशेष महत्व है। शास्त्र
में हर एक मास का विविध महत्व बताया हैं। आज हम
यहाँ एक ऐसे मास की बात करने जा रहे है जो
धनुर्मास के नाम से प्रसिद्ध है। इस मास का व्रत करने
ते हमें प्रभु की प्राप्ति होती है।

हमे प्रभु को प्राप्त करने के लिये धनुर्मास का व्रत
और धनुर्मास माहाल्य का अनुसंधान करना चाहिये।

हमारा प्रश्न यह है की, धनुर्मास क्या है? धनुर्मास
का महत्व कया है? इस धनुर्मास में मांगलिक कार्य क्‍यों
नहीं हो सकते? धनुर्मास मांगलिक कार्य के लिये
अशुभ और प्रभु आराधना के लिये उत्तम ऐसा क्‍यों?

धनुर्मास मांगलिक कार्य के लिए अशुभ और प्रभु
आराधना के लिए उत्तम ऐसा क्‍्यों?-

१७ दिसंवर से १४ जनवरी का एक मास का
ममय धनुर्मास के नाम से जाना जाता है। धनुर्मास में